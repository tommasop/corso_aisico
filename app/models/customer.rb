class Customer < ActiveRecord::Base
  validates :name, presence: true, length: { minimum: 3 }
  validates :email, presence: true, uniqueness: true
  # Descrivo le relazioni tra entità
  has_many :orders
end
