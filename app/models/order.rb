class Order < ActiveRecord::Base
  validates :code, :customer, :price, presence: true
  validates :code, uniqueness: true 
  # Descrivo le relazioni tra entità
  belongs_to :customer
  has_many :order_items
  has_many :products, through: :order_items

  def human_code
    "#{code} - #{customer.name}"
  end

  def total_price
    if order_items.present?
      final_prices = []
      order_items.each do | order_item |
        final_prices << (order_item.product.price - (order_item.product.price * (order_item.discount / 100))) * order_item.quantity
      end
      final_prices.sum
    else
      price
    end
  end
end
