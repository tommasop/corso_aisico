class OrderItem < ActiveRecord::Base
  belongs_to :product
  belongs_to :order

  def incremental_discounts
    case
    when (5..9).include?(quantity)
      (product.price - (product.price / 100 * 5)).to_f
    when (10..19).include?(quantity)
      (product.price - (product.price / 100 * 10)).to_f
    when quantity > 19 
      (product.price - (product.price / 100 * 20)).to_f
    else
      product.price
    end
  end

  def line_price
    to_discount = incremental_discounts * quantity
    to_discount - (to_discount / 100 * discount)
  end
end
