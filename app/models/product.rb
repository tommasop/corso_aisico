class Product < ActiveRecord::Base
  MATERIALS = ["METALLO", "LEGNO", "GOMMA", "PLASTICA", "CALCESTRUZZO"]
  has_many :order_items
  has_many :orders, through: :order_items
end
