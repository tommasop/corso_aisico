json.extract! order, :id, :code, :description, :price, :shipped_date, :customer_id, :created_at, :updated_at
json.url order_url(order, format: :json)
