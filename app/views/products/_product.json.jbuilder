json.extract! product, :id, :test, :operator, :material, :price, :va, :test_date, :created_at, :updated_at
json.url product_url(product, format: :json)