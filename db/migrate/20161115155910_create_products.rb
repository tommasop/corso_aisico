class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :test
      t.string :operator
      t.string :material
      t.decimal :price
      t.string :va
      t.date :test_date

      t.timestamps null: false
    end
  end
end
