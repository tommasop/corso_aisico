class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :code
      t.string :description
      t.decimal :price
      t.datetime :shipped_date

      # Connessione alla tabella customers
      # t.integer :customer_id
      t.references :customer

      t.timestamps null: false
    end
  end
end
