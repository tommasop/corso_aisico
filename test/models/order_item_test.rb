require 'test_helper'

class OrderItemTest < ActiveSupport::TestCase
  before do
    @oi = OrderItem.first
  end 

  describe "incremental_discounts" do
    it "must return full price quantity < 5" do
      @oi.quantity = 1
      @oi.incremental_discounts.must_equal @oi.product.price 
    end

    it "must apply a 5% discount for 5 to 9 quantity" do
      (5..9).each do | q |
        @oi.quantity = q
        @oi.incremental_discounts.must_equal (@oi.product.price - (@oi.product.price / 100 * 5)).to_f
      end
    end
    
    it "must apply a 10% discount for 10 to 19 quantity" do
      @oi.quantity = 10
      if (10..19).include?(@oi.quantity)
        @oi.incremental_discounts.must_equal (@oi.product.price - (@oi.product.price / 100 * 10)).to_f
      end
    end
    
    it "must apply a 20% discount for more than 19" do
      @oi.quantity = 20
      if @oi.quantity > 19
        @oi.incremental_discounts.must_equal (@oi.product.price - (@oi.product.price / 100 * 20)).to_f
      end
    end
  end

  describe "line_price" do
    it "returns the final order item price" do
      final_price = @oi.incremental_discounts * @oi.quantity
      @oi.line_price.to_f.must_equal (final_price - (final_price / 100 * @oi.discount))
    end
  end
end
