require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  describe "total_price" do
    it "must calculate order_items prices if present" do
      order = Order.first
      order.total_price.to_f.must_equal 5600.0
    end

    it "must return the order price if no order_items" do
      order = Order.second
      order.total_price.must_equal order.price
    end
  end
end
